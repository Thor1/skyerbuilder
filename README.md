# Skyer Builder for Unity

Skyer Builder is a tool created to help you build your game or application in Unity to multiple platforms with only one click.

## What exactly does Skyer Builder do?

The Skyer Builder creates a standard on top of the API provided by Unity to be easier to manipulate than the original, simplifying the build process from code, and making it possible to build multiple Unity Players or use an external UI like a command line or an Editor Window.

## How to use Skyer Builder?

You can use our window from Unity Editor, you can access it from `Window > Skyer Builder`. In the window, the process is very intuitive, you only choose a path by clicking on `...` and clicking on any checkbox, ending with only one click on the `Build` button to start the build process.

Another way to use the Skyer Builder is using code, in the [wiki of Skyer Builder in GitLab](https://gitlab.com/deersoftware/skyerbuilder/-/wikis/home), you can view the documentation on how to use it.

## My target isn't supported by Skyer Builder

If this occurs, you can do two things to solve this question. You can create an [Issue here](https://gitlab.com/deersoftware/skyerbuilder/-/issues) requesting official support for your target. You can also add support for your target forking or modifying locally the Skyer Builder source.

## How to download Skyer Builder?

You can install it from [Unity Asset Store](https://assetstore.unity.com/packages/slug/209104) or directly from the [repository on GitLab](https://gitlab.com/deersoftware/skyerbuilder), downloading the [Unity Package from here](https://gitlab.com/deersoftware/skyerbuilder/-/releases) or adding a package from the git URL "https://gitlab.com/deersoftware/skyerbuilder.git" in the Unity Package Manager.

## Credits

The Skyer Builder is created by [Nashira Deer](https://gitlab.com/NashiraDeer) and maintained by [DeerSoftware](https://www.deersoftware.dev/), licensed with the [MIT License](https://gitlab.com/deersoftware/skyerbuilder/-/blob/main/LICENSE.txt) to anyone who can use, modify and distribute your Skyer Builder.
