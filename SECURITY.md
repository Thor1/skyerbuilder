# Security Policy

## Supported Versions

| Version | > 2019.3 |  2019.4+           | > 2020.2 | 2020.3+            | > 2021.2 | 2021.3+            | 2022.1+            |
| :-----: | :------: | :----------------: | :------: | :----------------: | :------: | :----------------: | :----------------: |
|   1.0   | :x:      | :white_check_mark: | :x:      | :white_check_mark: | :x:      | :x:                | :x:                |
|   2.0   | :x:      | :x:                | :x:      | :x:                | :x:      | :white_check_mark: | :white_check_mark: |

## Reporting a Vulnerability

If you found any type of vulnerability, please report using our email [security@deersoftware.dev](mailto:security@deersoftware.dev) or using the [GitLab Issues](https://gitlab.com/deersoftware/skyerbuilder/-/issues).
